package Service;

import Ortega.JDBConnector.JDBConnector;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

@WebService(serviceName = "DBService")
public class DBService {
    /**
     *
     * @param ID
     * @param name
     * @param lastName
     * @return success
     */
    @WebMethod(operationName = "registerStudent")
    public boolean registerStudent(
            @WebParam(name = "ID") Integer ID, 
            @WebParam(name = "name") String name, 
            @WebParam(name = "lastName") String lastName){
        boolean success = true;
        
        try{
            JDBConnector conn = new JDBConnector(
            "localhost", 
            5432, 
            "postgres", 
            "masterkey", 
            "AnotherSchoolDB");
            
            conn.statement("INSERT INTO students(cid_stu, name_stu, lastname_stu) VALUES(?, ?, ?)", 
                ID, name, lastName);
        } catch(Exception e) {
            success = false;
        } finally {
            return success;
        }
    }
    
    /**
     *
     * @param ID
     * @param name
     * @param lastName
     * @return success
     */
    @WebMethod(operationName = "registerTeacher")
    public boolean registerTeacher(
            @WebParam(name = "ID") Integer ID, 
            @WebParam(name = "name") String name, 
            @WebParam(name = "lastName") String lastName){
        boolean success = true;
        
        try{
            JDBConnector conn = new JDBConnector(
            "localhost", 
            5432, 
            "postgres", 
            "masterkey", 
            "AnotherSchoolDB");
            
            conn.statement("INSERT INTO teachers(cid_tea, name_tea, lastname_tea) VALUES(?, ?, ?)", 
                ID, name, lastName);
        } catch(Exception e) {
            success = false;
        } finally {
            return success;
        }
    }
    
    /**
     *
     * @param desc
     * @return success
     */
    @WebMethod(operationName = "registerPeriod")
    public boolean registerPeriod(
            @WebParam(name = "description") String desc){
        boolean success = true;
        
        try{
            JDBConnector conn = new JDBConnector(
            "localhost", 
            5432, 
            "postgres", 
            "masterkey", 
            "AnotherSchoolDB");
            
            conn.statement("INSERT INTO periods(desc_per) VALUES (?)", 
                desc);
        } catch(Exception e) {
            success = false;
        } finally {
            return success;
        }
    }
    
    /**
     *
     * @param periodID
     * @param teacherID
     * @param section
     * @param name
     * @return success
     */
    @WebMethod(operationName = "registerCourse")
    public boolean registerCourse(
            @WebParam(name = "periodID") Integer periodID, 
            @WebParam(name = "teacherID") Integer teacherID, 
            @WebParam(name = "section") String section, 
            @WebParam(name = "name") String name){
        boolean success = true;
        
        try{
            JDBConnector conn = new JDBConnector(
            "localhost", 
            5432, 
            "postgres", 
            "masterkey", 
            "AnotherSchoolDB");
            
            conn.statement("INSERT INTO courses(id_per_cou, id_tea_cou, "
                + "section_cou, name_cou) VALUES(?, ?, ?, ?)", 
                periodID, teacherID, section, name);
        } catch(Exception e) {
            success = false;
        } finally {
            return success;
        }
    }
    
    /**
     *
     * @param studentID
     * @param courseID
     * @return success
     */
    @WebMethod(operationName = "inscribe")
    public boolean inscribe(
            @WebParam(name = "studentID") Integer studentID, 
            @WebParam(name = "courseID") Integer courseID){
        boolean success = true;
        
        try{
            JDBConnector conn = new JDBConnector(
            "localhost", 
            5432, 
            "postgres", 
            "masterkey", 
            "AnotherSchoolDB");
            
            conn.statement("INSERT INTO inscriptions(id_stu_ins, id_cou_ins) " +
            "SELECT id_stu, ? FROM students WHERE(cid_stu = ?)", 
                courseID, studentID);
        } catch(Exception e) {
            success = false;
        } finally {
            return success;
        }
    }
    
    @WebMethod(operationName = "searchByCousePeriod")
    public String[][] searchByCousePeriod(
            @WebParam(name = "courseID") Integer courseID, 
            @WebParam(name = "periodID") Integer periodID){
        try{
            JDBConnector conn = new JDBConnector(
            "localhost", 
            5432, 
            "postgres", 
            "masterkey", 
            "AnotherSchoolDB");
            
            conn.statement("SELECT cid_stu, name_stu, lastname_stu, " +
                "name_cou, section_cou, desc_per FROM students " +
                "JOIN inscriptions on id_stu = id_stu_ins " +
                "JOIN courses on id_cou = id_cou_ins " +
                "JOIN periods on id_per_cou = id_per " +
                "WHERE id_cou = ? AND id_per = ?", 
                courseID, periodID);
            
            return (String[][]) conn.result;
        } catch(Exception e) {
            return null;
        }
    }
}